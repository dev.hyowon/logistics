(function (w, d) {
  // init
  uiGnb(d);
  keyUpByte(d);
  modalControl(d);
  mobileGnb();
  mobileDropdown();
  mainMotion();
  sitemapFloting();
})(window, document, undefined);

/**
 *
 * @param { document } document
 */
function uiGnb(document) {
  // main일 경우 동작 X
  if (document.querySelector('.main__visual')) return;
  const gnbWrap = document.querySelector('.app-aside__menu');
  const gnbItem = gnbWrap.querySelectorAll('.app-aside__menu--item');
  // event bind
  gnbItem.forEach((item) => {
    item.children[0].addEventListener(
      'click',
      () => {
        gnbItem.forEach((liElement) => {
          liElement.classList.remove('is-active');
        });
        item.children[0].closest('li').classList.add('is-active');
      },
      false
    );
  });
}

/**
 *
 * @param {*} document
 */
function keyUpByte(document) {
  if (!document.querySelector('[data-js="textByte"]')) return;
  const textForm = document.querySelector('[data-js="textByte"]');
  const textFormByte = document.querySelector('[data-js="textByteElement"]');
  const currentByte = textFormByte.querySelector('.byte__current');
  const maxLength = parseInt(textForm.attributes.maxlength.value);
  const maxByte = textFormByte.querySelector('.byte__length');
  maxByte.innerText = maxLength;
  textForm.addEventListener(
    'keyup',
    (e) => {
      const currentLength = textForm.value.length;
      currentByte.innerText = currentLength;
      if (currentLength >= maxLength) {
        console.log('초과');
      }
    },
    false
  );
}

function modal() {
  if (!document.querySelector('.modal__container')) return;
  // initialize modal element
  const modalEl = document.createElement('div');
  const modalContent = document.querySelector('.modal__container');
  const modalContentClone = modalContent.cloneNode(true);
  modalEl.append(modalContentClone);
  modalEl.classList.add('modal', 'is-modal-open');
  // show modal
  mui.overlay('on', modalEl);

  // login, join modal
  if (modalContent.classList.contains('modal--users')) {
    const userModal = modalEl.parentElement;
    userModal.classList.add('is-user-modal');
  }

  const closeButton = modalContentClone.querySelector('[data-js="modalClose"]');
  closeButton.addEventListener('click', modalClose, false);
}

function modalClose() {
  // show modal
  mui.overlay('off');
}

/**
 * modal - open
 * @param {*} document
 */
function modalControl() {
  if (!document.querySelector('[data-js="modal"]')) return;
  const modalButton = document.querySelector('[data-js="modal"]');
  modalButton.addEventListener('click', modal, false);
}

/**
 * mobile gnb toggle
 */
function mobileGnb() {
  if (!document.querySelector('.app-aside')) return;
  const aside = document.querySelector('.app-aside');
  const btnHamburger = document.querySelector('.btn-hamburger');
  btnHamburger.addEventListener(
    'click',
    () => {
      aside.classList.toggle('is-gnb-open');
    },
    false
  );
}

/**
 * dropbox menu
 */
function mobileDropdown() {
  if (!document.querySelector('.app-nav')) return;
  const appNav = document.querySelector('.app-nav');
  const appNavBtn = appNav.querySelector('.app-nav__dropdown');
  let drodownMenus;
  appNavBtn.addEventListener(
    'click',
    () => {
      appNav.classList.toggle('is-dropdown-open');
      drodownMenus = document.querySelectorAll(
        '.is-dropdown-open .mui-tabs__bar li'
      );
      drodownMenus.forEach((item) => {
        item.children[0].addEventListener(
          'click',
          (e) => {
            drodownMenus.forEach((liElement) => {
              liElement.classList.remove('mui--is-active');
            });
            item.classList.add('mui--is-active');
            appNav.classList.remove('is-dropdown-open');
            appNavBtn.innerText = item.children[0].innerText;
            e.preventDefault();
          },
          false
        );
      });
    },
    false
  );
}

/**
 * main motion
 */
function mainMotion() {
  if (!document.querySelector('.main__visual')) return;
  const objectWrap = document.querySelector('.main__object');
  const paneItems = document.querySelectorAll('.main__pane--item a');
  // pc
  paneItems.forEach((item) => {
    item.addEventListener(
      'mouseover',
      () => {
        const paneClass = item.parentElement.classList;
        if (paneClass.contains('is-pane-01')) {
          objectWrap.classList.add('pane-motion01');
        } else if (paneClass.contains('is-pane-02')) {
          objectWrap.classList.add('pane-motion02');
        } else {
          objectWrap.classList.add('pane-motion03');
        }
      },
      true
    );
    item.addEventListener(
      'mouseout',
      () => {
        const paneClass = item.parentElement.classList;
        if (paneClass.contains('is-pane-01')) {
          objectWrap.classList.remove('pane-motion01');
        } else if (paneClass.contains('is-pane-02')) {
          objectWrap.classList.remove('pane-motion02');
        } else {
          objectWrap.classList.remove('pane-motion03');
        }
      },
      true
    );
  });
}

/**
 * sitemap floting
 */
function sitemapFloting() {
  if (!document.querySelector('.btn-sitemap')) return;
  const btnSitemap = document.querySelector('.btn-sitemap');
  const sitemapDim = document.querySelector('.dim');
  const btnClose = document.querySelector('.btn-sitemap-close');
  btnSitemap.addEventListener('click', open, false);
  sitemapDim.addEventListener('click', close, false);
  btnClose.addEventListener('click', close, false);

  function open(e) {
    e.preventDefault();
    document.body.classList.add('is-sitemap-open');
  }
  function close() {
    document.body.classList.remove('is-sitemap-open');
  }
}
