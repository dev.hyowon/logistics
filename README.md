# logistics

## web server

> 절대로 경로로 세팅해놨기때문에 웹 서버로 실행하여 봐주십시요. <br>
> [URL]: https://www.npmjs.com/package/http-server

```js
npm install http-server

http-server
```

## 퍼블리싱 현황판

```html
/status.html
```

## MATERIAL Ui - SASS

URL: https://github.com/rubysamurai/mui-sass  
URL: https://www.muicss.com
